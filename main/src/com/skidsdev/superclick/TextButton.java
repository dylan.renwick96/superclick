package com.skidsdev.superclick;

import java.util.*;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

public class TextButton extends Button
{
    String buttonText;

    BitmapFont textFont;

    public TextButton(FileHandle file, BitmapFont font, String text)
    {
        this(file, font);
        buttonText = text;
    }
    public TextButton(FileHandle file, BitmapFont font)
    {
        super(file);
        textFont = font;
    }

    public void setText(String text)
    {
        buttonText = text;
    }

    @Override
    public void update(float delta)
    {
        if (super.lerpForwards)
        {

        }
        else if (super.lerpBackwards)
        {
            
        }
    }

    @Override
    public void draw(Batch batch)
    {
        super.draw(batch);
        GlyphLayout layout = new GlyphLayout(textFont, buttonText);
        float fontX = super.getX() + (super.getWidth() - layout.width) / 2;
        float fontY = super.getY() + (super.getHeight() + layout.height) / 2;

        textFont.draw(batch, layout, fontX, fontY);
    }
}