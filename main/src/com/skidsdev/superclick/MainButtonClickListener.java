package com.skidsdev.superclick;

import java.util.*;
import com.badlogic.gdx.Gdx;

public class MainButtonClickListener implements ClickListener
{
    private MainScreen screen;

    public MainButtonClickListener(MainScreen screen)
    {
        this.screen = screen;
    }

    public void onClick(Button button)
    {
        screen.clickCount += screen.gainPerClick;
    }
}
