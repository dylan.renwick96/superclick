package com.skidsdev.superclick;

import com.badlogic.gdx.Game;

public class SuperClickGame extends Game
{
    private MainScreen ms; 

    @Override
    public void create()
    {
        ms = new MainScreen(this);
        this.setScreen(ms);
    }
}
