package com.skidsdev.superclick;

import java.util.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Button extends Sprite
{
    private final float LERP_TIME = 0.1f;

    private float lerpTimer = 0;
    private boolean lerpForwards;
    private boolean lerpBackwards;

    private Color baseColor;
    private Color highlightColor;

    private List<ClickListener> clickListeners;

    public Button(FileHandle file)
    {
        this(new Texture(file));
    }
    public Button(Texture texture)
    {
        super(texture);
        baseColor = super.getColor();
        highlightColor = new Color(0, 1, 0, 1).lerp(Color.WHITE, 0.25f);
        clickListeners = new ArrayList<ClickListener>();
    }

    public void addListener(ClickListener listener)
    {
        clickListeners.add(listener);
    }

    public void onClick()
    {
        Gdx.app.log("INFO", "Button click!");
        for(ClickListener listener : clickListeners)
        {
            listener.onClick(this);
        }
        lerpForwards = true;
    }

    public void update(float delta)
    {
        if (lerpForwards)
        {
            lerpTimer += delta;
            super.setColor(baseColor.lerp(highlightColor, lerpTimer / LERP_TIME));
            if (lerpTimer >= LERP_TIME)
            {
                Gdx.app.log("INFO", "Switching lerp direction");
                lerpTimer = 0;
                lerpForwards = false;
                lerpBackwards = true;
            }
        }
        else if (lerpBackwards)
        {
            lerpTimer += delta;
            super.setColor(baseColor.lerp(Color.WHITE, lerpTimer / LERP_TIME));
            if (lerpTimer >= LERP_TIME)
            {
                lerpTimer = 0;
                lerpBackwards = false;
            }
        }
    }

    public boolean isButtonClick(int x, int y)
    {
        Rectangle rect = new Rectangle(super.getX(), super.getY(), super.getWidth(), super.getHeight());
        return Util.checkOverlap(x, y, rect);
    }
}