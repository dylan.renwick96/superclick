package com.skidsdev.superclick;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class ButtonInputHandler implements InputProcessor
{
    private MainScreen screen;

    public ButtonInputHandler(MainScreen screen)
    {
        this.screen = screen;
    }

    public boolean keyDown (int keycode) 
    {
        return false;
    }

    public boolean keyUp (int keycode)
    {
        return false;
    }

    public boolean keyTyped (char character)
    {
        return false;
    }

    public boolean touchDown (int x, int y, int pointer, int button)
    {
        Gdx.app.log("INFO", "Button click detected");
        if (pointer == Input.Buttons.LEFT)
        {
            Gdx.app.log("INFO", "Left click");
            return screen.onClick(x, y);
        }

        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button)
    {
        return false;
    }

    public boolean touchDragged (int x, int y, int pointer)
    {
        return false;
    }

    public boolean mouseMoved (int x, int y)
    {
        return false;
    }

    public boolean scrolled (int amount)
    {
        return false;
    }
}