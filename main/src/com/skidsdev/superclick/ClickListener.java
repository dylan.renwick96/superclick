package com.skidsdev.superclick;

import java.util.*;

public interface ClickListener
{
    void onClick(Button button);
}
