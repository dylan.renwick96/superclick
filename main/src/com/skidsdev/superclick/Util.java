package com.skidsdev.superclick;

import com.badlogic.gdx.math.Rectangle;

public class Util
{
	public static boolean checkOverlap(int x, int y, Rectangle rect)
    {
        return ((x > rect.x && y > rect.y) && (x < rect.x + rect.width && y < rect.y + rect.height));
    }
}