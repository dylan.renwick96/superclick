package com.skidsdev.superclick;

import java.util.*;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.Screen;

public class MainScreen implements Screen
{
    SuperClickGame g;
    SpriteBatch spb;
    OrthographicCamera cam;

    ArrayList<Button> buttons;
    BitmapFont font;

    public float clickCount;
    public float gainPerClick;

    public MainScreen(SuperClickGame g)
    {
        this.g = g;
        gainPerClick = 1;

        buttons = new ArrayList<Button>();

        spb = new SpriteBatch();    

        cam = new OrthographicCamera();
        cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.update(true);

        font = new BitmapFont(Gdx.files.internal("assets/font/easterbuns.fnt"),
            Gdx.files.internal("assets/font/main_0.png"), false);

        Button clickButton = new TextButton(Gdx.files.internal("assets/img/button.png"), font, "Click me!");
        clickButton.setPosition(300, 199);
        clickButton.addListener(new MainButtonClickListener(this));
        buttons.add(clickButton);

        ButtonInputHandler ih = new ButtonInputHandler(this);
        Gdx.input.setInputProcessor(ih);
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        spb.setProjectionMatrix( cam.combined );
        spb.begin();
        for(Button b : buttons)
        {
            b.update(delta);
            b.draw(spb);
        }
        drawCenteredText(spb, font, "Clicks: " + (int)clickCount, 400, 140);
        spb.end();
    }

    private void drawCenteredText(SpriteBatch batch, BitmapFont font, String text, int x, int y)
    {
        GlyphLayout layout = new GlyphLayout(font, text);

        float fontX = x - (layout.width / 2);
        float fontY = y - (layout.height / 2);

        font.draw(batch, layout, fontX, fontY);
    }

    public boolean onClick(int x, int y)
    {
        boolean retVal = false;

        for (Button b : buttons)
        {
            if (b.isButtonClick(x, y))
            {
                b.onClick();
                if (!retVal) retVal = true;
            }
        }

        return retVal;
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void dispose()
    {

    }
}
