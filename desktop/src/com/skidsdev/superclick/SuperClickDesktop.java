package com.skidsdev.superclick;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class SuperClickDesktop
{
    public static void main(String[] args)
    {
        LwjglApplicationConfiguration cf = new LwjglApplicationConfiguration();
        cf.title = "SuperClick";
        cf.useGL30 = true;
        cf.width = 800;
        cf.height = 480;

        new LwjglApplication(new SuperClickGame(), cf);
    }
}
